import argparse
import numpy as np
import cv2
import skimage.io as io

import json
import shutil
import os

import signal_aspect_detection
from signal_aspect_detection.signal_class import load_signal
from signal_aspect_detection import find_matches
from signal_aspect_detection import find_aspect

from signal_aspect_detection.find_polygones import *

def parse_arguments():
    parser = argparse.ArgumentParser(description='Process signal aspect detection on images.')
    parser.add_argument("--model", required=True,
                            metavar="MODEL",
                            help="model e.g. ampel, kshauptsignal, kshauptsignal2")
    parser.add_argument("--dataset", required=True,
                            metavar="NAME",
                            help="dataset name")
    parser.add_argument("--quadrant", nargs="?",
                            metavar="QUADRANT", default=-1, type=int,
                            help="where the signal must be located")
    parser.add_argument("--min-height", nargs="?" , metavar="MINHEIGHT", default=-1, type=int,
                            help="min height of detected signal")
    args = parser.parse_args()
    return args.model , args.dataset , args.quadrant , args.min_height

def table(signalLights,lights):
    head = ["CircleRadius","Expansion","Flatness","Color","Position","Match"]
    body = []
    for i in range(len(signalLights)):
        signalLight = signalLights[i]
        (x,y) = signalLight.getXY()
        row = [
            "{:0.2f}".format(signalLight.getCircleRadius()),
            "{:0.2f}".format(signalLight.getExpansion()),
            "{:0.2f}".format(signalLight.getFlatness()),
            signalLight.getColor(),
            "{:0.2f},{:0.2f}".format(x,y),
            str(lights[i])
        ]

        body.append(row)

    return { "head" : head , "body" : body}


def copy_and_overwrite(from_path, to_path):
    if os.path.exists(to_path):
        shutil.rmtree(to_path)
    shutil.copytree(from_path, to_path)


def get_quadrants(box,height , width):
    quadrants = []

    # 2 | 1
    #-------
    # 3 | 4 
    if box['right'] > width/2 and box['top'] < height/2: quadrants.append(1)
    if box['right'] > width/2 and box['bottom'] > height/2: quadrants.append(4)
    if box['left'] < width/2 and box['top'] < height/2: quadrants.append(2)
    if box['left'] < width/2 and box['bottom'] > height/2: quadrants.append(3)
    return quadrants

#los geht's
model , dataset , quadrant , min_height = parse_arguments();

# init report folder
copy_and_overwrite("reports/template", "reports/{}".format(dataset))

#read model
model_path = 'models/signals/{}.json'.format(model)
signal_definition = load_signal(model_path)


result = []
ix = 0

with open("data/{}/tmp/traffic_light.jsonl".format(dataset)) as f:
    Lines = f.readlines()
    for line in Lines:
        detection = json.loads(line)
        for gbox in detection['boxes']:
            for cl in gbox['classes']:
                if cl['name'] != "traffic light": continue
                ix +=1
                try:
                    video_frame = io.imread("data/{}/{}".format(dataset,detection['file']))
                    video_height , video_width , _ = video_frame.shape

                    box = gbox['box']

                    height_box = box['bottom'] - box['top']
                    quadrants = get_quadrants(box,video_height , video_width);

                    print("file",detection['file'],"height_box",height_box,"quadrants",quadrants)

                    if min_height != -1 and min_height >= height_box: continue
                    if quadrant != -1 and quadrant not in quadrants: continue

                    #original = video_frame.crop((box['left'],box['top'],box['right'],box['bottom']))
                    original = np.copy(video_frame[ box['top']:box['bottom'], box['left']:box['right']])

                    cv2.rectangle(video_frame, (box['left'],box['top']), (box['right'],box['bottom']), (255,0,0), 2)

                    # resize image
                    original_height , original_width , _ = original.shape
                    width = 100 
                    height = int(original_height * width / original_width)
                    dim = (width, height)
                    I = cv2.resize(original, dim, interpolation = cv2.INTER_AREA)
                    

                    binary_img = signal_aspect_detection.create_binary(I,40)
    
                    #crop image
                    rows , cols , _ = I.shape
                    (left,right,top,bottom) = signal_aspect_detection.crop_signal2(I)
                    #print(file,"crop",(left,right,top,bottom))

                    #fix crop if not found
                    if left == -1: left = 0
                    if right == -1: right = cols
                    if top == -1: top = 0
                    if bottom == -1: bottom = rows
                    
                    crop_img = I[top:bottom, left:right]


                    #detect polygones
                    polygones = detect_polygones(binary_img)
                    print(len(polygones)," polygones detected")
                         
                    polygon_img = np.zeros((I.shape[0], I.shape[1], 3), dtype = "uint8")
                    paint_polygones(polygon_img,polygones)


                    #filter color (TODO: duplicate ??)
                    img = signal_aspect_detection.filter_by_color2(crop_img)

                    signalLights = signal_aspect_detection.find_lights(img,crop_img)
                    #print("found",len(signalLights),"signalLigths")

                    rows , cols , _ = crop_img.shape

                    lights = find_matches(signalLights,cols,signal_definition)
                    match = find_aspect(lights,signal_definition)
                    print(box,"=>",'"{}"'.format(match))

                    data = {
                    "title" : detection['file'],
                    "match": match,
                    "details" : table(signalLights,lights),
                        "images" : {
                                "video_frame" : "images/{:02d}_video_frame.png".format(ix),
                                "original" : "images/{:02d}_original.png".format(ix),
                                "cropped" : "images/{:02d}_cropped.png".format(ix),
                                "binary": "images/{:02d}_binary.png".format(ix),
                                "polygones" : "images/{:02d}_polygones.png".format(ix),
                                "filtered" : "images/{:02d}_filtered.png".format(ix),
                                "ellipses" : "images/{:02d}_ellipses.png".format(ix),
                                "matched" : "images/{:02d}_matched.png".format(ix),
                                "model" : "images/{:02d}_model.png".format(ix)
                        }
                    }
                    result.append(data)


                    io.imsave("reports/{}/{}".format(dataset,data['images']['video_frame']),video_frame)

                    io.imsave("reports/{}/{}".format(dataset,data['images']['original']),original)
                    io.imsave("reports/{}/{}".format(dataset,data['images']['cropped']),crop_img)
                    io.imsave("reports/{}/{}".format(dataset,data['images']['polygones']),polygon_img)
                    io.imsave("reports/{}/{}".format(dataset,data['images']['binary']),binary_img)
                    io.imsave("reports/{}/{}".format(dataset,data['images']['filtered']),img)



                    COLORS = {}
                    COLORS["red"] = (255,0,0)
                    COLORS["yellow"] = (255,255,0)
                    COLORS["green"] = (0,255,0)
                    COLORS["white"] = (255,255,255)
                    COLORS["black"] = (0,0,0)
                    COLORS["blue"] = (0,255,255)

                    nimg = np.zeros([rows , cols,3],dtype=np.uint8)
                    eimg = np.zeros([rows , cols,3],dtype=np.uint8)
                    eimg.fill(200) # or img[:] = 255

                    for i in range(len(signalLights)):
                        signalLight = signalLights[i]
                        color_class , _ = signalLight.getColor()[0]
                        color = COLORS[color_class]
                        cv2.ellipse(eimg, signalLight.getEllipse(), color , 2)

                        if lights[i]:
                            definition = lights[i][0][1]
                            color_class = definition['color']
                            color = COLORS[color_class]
                            (x,y) = signalLight.getXY() 
                            r = round(signalLight.getExpansion() / 2)
                            cv2.circle(nimg, (round(x) , round(y)), r , color , -1)
                            #print(x/cols,y/cols,r/cols,color_class)

                    io.imsave("reports/{}/{}".format(dataset,data['images']['ellipses']),eimg)
                    io.imsave("reports/{}/{}".format(dataset,data['images']['matched']),nimg)


                    signal_image = np.zeros([100,50,3],dtype=np.uint8)
                    if match:
                        signal_definition.draw_signal_aspect(signal_image,(0,0), 50 ,match)
                    io.imsave("reports/{}/{}".format(dataset,data['images']['model']),signal_image)
                except Exception as e:
                    print(e)

gen = {
    "dataset" :dataset
}

with open("reports/{}/data/result.js".format(dataset), 'w') as f:
    f.write("var generate = {};".format(json.dumps(gen)))
    f.write("var data = {};".format(json.dumps(result)))

