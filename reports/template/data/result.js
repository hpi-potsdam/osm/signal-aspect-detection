var generate = {
    "dataset" : "Testdataset 1"
}

var data = [
    { 
      "title" : "Testset 1 (generated)",
      "match": "HP 0 + ZS7",
      "details" : {
        "head" : ["CircleRadius","Expansion","Flatness","Color","Position","Match"],
        "body" : [
          ["5.53",6.83,1.53,"('yellow', 1.0)]","36.00,116.50","[('l83', {'x': 0.5, 'y': 1.56, 'radius': 0.08, 'color': 'yellow'}, 0.01969267160210468, 0.5904009040065147, 0.0)]"],
          ["5.53","6.83","1.53","('yellow', 1.0)]","36.00,116.50","[('l83', {'x': 0.5, 'y': 1.56, 'radius': 0.08, 'color': 'yellow'}, 0.01969267160210468, 0.5904009040065147, 0.0)]"],
        ]
      },
      "images" : {
        "video_frame" : "images/video_frame_large.png",
        "original" : "images/original.png",
        "cropped" : "images/cropped.png",
        "filtered" : "images/filtered.png",
        "ellipses" : "images/ellipses.png",
        "matched" : "images/matched.png",
        "model" : "images/model.png"
      }
    },
    { 
      "title" : "Testset 1 (generated)",
      "match": "HP 0 + ZS7",
      "details" : {
        "head" : ["CircleRadius","Expansion","Flatness","Color","Position","Match"],
        "body" : [
          ["5.53","6.83","1.53","('yellow', 1.0)]","36.00,116.50","[('l83', {'x': 0.5, 'y': 1.56, 'radius': 0.08, 'color': 'yellow'}, 0.01969267160210468, 0.5904009040065147, 0.0)]"],
          ["5.53","6.83","1.53","('yellow', 1.0)]","36.00,116.50","[('l83', {'x': 0.5, 'y': 1.56, 'radius': 0.08, 'color': 'yellow'}, 0.01969267160210468, 0.5904009040065147, 0.0)]"],
        ]
      },
      "images" : {
        "video_frame" : "images/video_frame.png",
        "original" : "images/original.png",
        "cropped" : "images/cropped.png",
        "filtered" : "images/filtered.png",
        "ellipses" : "images/ellipses.png",
        "matched" : "images/matched.png",
        "model" : "images/model.png"
      }
    }
  ];

