# signal aspect detection

Detect signal aspects 

![Process of signal aspect detection](docs/process.png "Signal aspect detection")

## Installation

### Use Docker-Container

Using the VSCode *Remote - Containers* extension and run it in container. 

### Install for develop
```
cd src
python setup.py develop
```

## test
```
pytest tests/
```

# use print
```
pytest tests/test_find_lights.py -s
```

#### Source Distribution
```
cd src;
python setup.py sdist
```

#### Upload to Artifactory
```
twine upload dist/*
```

For signin automaticly you can provide artifactory credentials via TWINE environment
```
export TWINE_USERNAME= 
export TWINE_PASSWORD=
```
## evaluation

```
python evaluation.py --model ampel --dataset evaluation-ampel
```
```
python evaluation.py --model ampel --dataset blinken
```

```
python evaluation.py --model kshauptsignal3 --dataset evaluation-kshauptsignal3
```

```
python evaluation.py --model kshauptsignal3 --dataset drohne
```

``` 
python evaluation.py --model kshauptsignal2 --dataset 20210514BPAFLL593video900pmp4  
```


### falsches Model
```
python evaluation.py --model h_v_hauptsignal --dataset 20210619RKTFS70588video900pmp4  
```

### filter results
``` 
python evaluation.py --model kshauptsignal3 --quadrant 1 --min-height 50 --dataset 20211006LMLS63460videomp4  
```

## Start jupiter notebook
```
cd jupyter
jupyter notebook --allow-root
```
