# Details


## crop signal

Scannline algorithm that determines the black percentage (signal aspect background) per row/column. ([1]/[2])
Ignores areas that do not reach a minimum thickness.

![crop signal](docs/crop_signal.jpg)

```
import signal_aspect_detection
import skimage.io as io

I = io.imread("image.png")
(left,right,top,bottom) = signal_aspect_detection.crop_signal(I)

```

## Signal light detection

1. color_filter - converts image to specific colors, grey and white for better edge detection [3]
```
img = signal_aspect_detection.filter_by_color(crop_img)
```
2. find_ellipses - detects ellipses in color filtered image, uses cv methods ( Canny, findContours ) [4]
3. get_ellipse_color - clusters color pixels inside the ellipse 

## Signal aspect detection

 1. find_matches - find for each ellipse a match with bulp in signal model [5]
 2. find_aspect - compare glowing bulbs with defined aspect in model and find match [6]

![signal-aspect-detection](docs/signal-aspect-detection.png)
