
def test_diff_radius():
    from signal_aspect_detection.signal_matching import diff_radius
    import math 
    assert 0 == diff_radius(1,1)
    assert math.isclose(diff_radius(7,3),diff_radius(3,7))
    assert diff_radius(1,100) < diff_radius(1,110) + diff_radius(110,100)
    assert 2 == diff_radius(0.1,1)

def test_diff_xy():
    from signal_aspect_detection.signal_matching import diff_xy
    import math 

    p1 = (1,1)
    p2 = (2,2)
    p3 = (3,3)
    assert 0 == diff_xy(p1,p1)
    assert diff_xy(p1,p2) == diff_xy(p2,p1)
    assert diff_xy(p1,p2) < diff_xy(p1,p3) + diff_xy(p3,p2)
    assert math.sqrt(2) == diff_xy(p1,p2)

def test_diff_color():
    from signal_aspect_detection.signal_matching import diff_color
    
    assert 0.06666666666666667 == diff_color("yellow",[('yellow', 0.9333333333333332), ('red', 0.06666666666666667)])
    assert 0.9999999999999999 == diff_color("yellow",[('black', 0.9333333333333332), ('white', 0.06666666666666667)])

def test_find_matches():
    from signal_aspect_detection import find_matches
    from signal_aspect_detection.signal_class import load_signal
    from signal_aspect_detection.light_class import SignalLight

    signalLigths = []
    ellipse1 = ((160.89816284179688, 145.66734313964844), (64.5445556640625, 66.75177001953125), 84.88407135009766)
    signalLight1 = SignalLight(ellipse1,[("red", 0.85),("black",0.15)])
    signalLigths.append(signalLight1)
    
    ellipse2 = ((160.89816284179688, 145.66734313964844), (5, 5), 0)
    signalLight2 = SignalLight(ellipse2,[("yellow", 0.85),("black",0.15)])
    signalLigths.append(signalLight2)

    signal = load_signal("models/signals/kshauptsignal.json")
    lights = find_matches(signalLigths,305,600,signal)

    assert 2 == len(lights)
    assert "l1" == lights[0][0][0]



def test_find_aspect():
    from signal_aspect_detection import find_aspect
    from signal_aspect_detection.signal_class import load_signal

    signal1 = load_signal("models/signals/kshauptsignal.json")
    signal2 = load_signal("models/signals/kshauptsignal2.json")
 
    lights1 = [
       ("l1",0.1,0.1,0.1)
    ]
    assert 'Hp 0' == find_aspect(lights1,signal1)
    assert 'Hp 0' == find_aspect(lights1,signal2)

    lights2 = [
        ("l81",0.1,0.1,0.1),("l1",0.1,0.1,0.1), ("l83",0.1,0.1,0.1),("l82",0.1,0.1,0.1)
    ]
    assert None == find_aspect(lights2,signal1)
    assert 'Hp 0+Zs 7' == find_aspect(lights2,signal2)