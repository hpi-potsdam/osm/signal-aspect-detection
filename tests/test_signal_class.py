



def test_SignalClass():
    from signal_aspect_detection.signal_class import load_signal
    import numpy as np

    signal = load_signal("models/signals/kshauptsignal.json")
    assert 8 == len(signal.get_aspects())
    assert 6 == len(signal.get_lights())

    signal_image = np.zeros([100,50,3],dtype=np.uint8)
    signal.draw_signal_aspect(signal_image,(0,0), 50 ,"Hp 0")
