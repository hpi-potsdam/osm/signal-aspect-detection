


def test_crop_signal():
    from signal_aspect_detection import crop_signal
    import numpy as np
    import cv2

    # height , width
    img = np.zeros([100,50,1],dtype=np.uint8)
    # x , y
    cv2.rectangle(img, (0,0), (49,99) , (0), -1)
    cv2.rectangle(img, (10,20), (45,75) , (255), -1)
    cv2.rectangle(img, (10,80), (45,85) , (255), -1)

    (left,right,top,bottom) = crop_signal(img)
    assert (10,45,20,75) == (left,right,top,bottom)