import numpy as np
import cv2


def test_get_pixel_color_class_red():
    from signal_aspect_detection.color_detector import get_pixel_color_class
    assert "red" == get_pixel_color_class(np.array([255 , 0 , 0 , 255],dtype=np.uint8))

def test_get_pixel_color_class_green():
    from signal_aspect_detection.color_detector import get_pixel_color_class
    assert "green" == get_pixel_color_class(np.array([0 , 255 , 0 , 255],dtype=np.uint8))

def test_get_pixel_color_class_blue():
    from signal_aspect_detection.color_detector import get_pixel_color_class
    assert "blue" == get_pixel_color_class(np.array([0 , 0 , 255 , 255],dtype=np.uint8))

def test_get_lab_color_class_yellow():
    from signal_aspect_detection.color_detector import get_lab_color_class
    assert "yellow" == get_lab_color_class(43.56197718262972, 33.19566802715038,52.466270118503004)
    
def test_get_lab_color_class_red():
    from signal_aspect_detection.color_detector import get_lab_color_class
    assert "red" == get_lab_color_class(24.31485286401172, 39.74940499020519, 35.68255065204981)

