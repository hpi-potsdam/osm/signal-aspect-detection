
def test_HorizontalImageAccessor():
    from signal_aspect_detection.image_accessor_class import HorizontalImageAccessor
    import numpy as np

    height = 100
    width = 55
    img = np.zeros([height , width , 3],dtype=np.uint8)
    imageAccessor = HorizontalImageAccessor(img)
    img[17,5] = (1 , 2 , 3)
    img[5,17] = (3 , 4 , 5)

    assert 55 == imageAccessor.get_line_length()
    assert 100 == imageAccessor.get_lines_cnt()
    assert (1 , 2 , 3) == imageAccessor.get_rgb(17,5)

def test_VerticalImageAccessor():
    from signal_aspect_detection.image_accessor_class import VerticalImageAccessor
    import numpy as np

    height = 100
    width = 55
    img = np.zeros([height , width , 3],dtype=np.uint8)
    img[17,5] = (1 , 2 , 3)
    img[5,17] = (3 , 4 , 5)

    imageAccessor = VerticalImageAccessor(img)
    assert 100 == imageAccessor.get_line_length()
    assert 55 == imageAccessor.get_lines_cnt()
    assert (3 , 4 , 5) == imageAccessor.get_rgb(17,5)