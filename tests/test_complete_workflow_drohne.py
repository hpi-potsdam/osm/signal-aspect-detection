import pytest
import numpy as np
import cv2
import skimage.io as io


from signal_aspect_detection.signal_class import load_signal
from signal_aspect_detection import detect_lights
from signal_aspect_detection import filter_matches
from signal_aspect_detection import find_aspect




@pytest.mark.parametrize("image,model,signal_aspect", [
        ('data/drohne-signals/traffic_light0001.png','models/signals/kshauptsignal3.json','Aus'),
        ('data/drohne-signals/traffic_light0002.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0003.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0004.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0005.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0006.png','models/signals/kshauptsignal3.json','Ks 1'),
        ('data/drohne-signals/traffic_light0007.png','models/signals/kshauptsignal3.json','Hp 0+Zs 1'),
        ('data/drohne-signals/traffic_light0008.png','models/signals/kshauptsignal3.json','Hp 0+Zs 1'),
        ('data/drohne-signals/traffic_light0009.png','models/signals/kshauptsignal3.json','Hp 0+Sh 1'),
        ('data/drohne-signals/traffic_light0010.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0011.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0012.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0013.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0014.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0015.png','models/signals/kshauptsignal3.json','Ks 2'),
        ('data/drohne-signals/traffic_light0016.png','models/signals/kshauptsignal3.json','Ks 2'),
        ('data/drohne-signals/traffic_light0017.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0018.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0019.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0020.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0021.png','models/signals/kshauptsignal3.json','Hp 0'),
        ('data/drohne-signals/traffic_light0022.png','models/signals/kshauptsignal3.json','Hp 0+Zs 1'),
        ('data/drohne-signals/traffic_light0023.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0024.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0025.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0026.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0027.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0028.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0029.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0030.png','models/signals/kshauptsignal3.json','Hp 0+Zs 7'),
        ('data/drohne-signals/traffic_light0031.png','models/signals/kshauptsignal3.json','Ks 2+vBw'),
        ('data/drohne-signals/traffic_light0032.png','models/signals/kshauptsignal3.json','Ks 2+vBw'),
        ('data/drohne-signals/traffic_light0033.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0034.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0035.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0036.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/drohne-signals/traffic_light0037.png','models/signals/kshauptsignal3.json','Ks 2+Vwh'),
        ('data/gen-signals/kshauptsignal2_Ks2_vBw.png','models/signals/kshauptsignal2.json','Ks 2+vBw'),
        ('data/ks-signals/traffic_light0003.png','models/signals/ampel.json','yellow + red'),
        ('data/ks-signals/traffic_light0007.png','models/signals/ampel.json','red'),
        ('data/ks-signals/traffic_light0008.png','models/signals/ampel.json','yellow'),
        ('data/ks-signals/traffic_light0002.png','models/signals/ampel.json','green'),
        ('data/ks-signals/traffic_light0006.png','models/signals/ampel.json','red')
    ])

def test(image,model,signal_aspect):

    print("image",image)
    I = io.imread(image)

    print("model",model)
    signal_definition = load_signal(model)

    signalLights ,  lights_matches , images  = detect_lights(I,signal_definition)
    
    #dump
    for i in range(len(signalLights)):
        signalLight = signalLights[i]
        print("Light",i,signalLight.getXY(),signalLight.getExpansion(),signalLight.getColor())
        for lm in lights_matches[i]:
            print("     match",lm)

    lights_filtered = filter_matches(lights_matches,0.15,0.7,0.8)
    print("lights_filtered",lights_filtered)
    matching = find_aspect(lights_filtered,signal_definition)  
    print("matching",matching)
    assert signal_aspect == matching
