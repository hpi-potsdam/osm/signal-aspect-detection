


def test_find_lights():
    from signal_aspect_detection import find_lights
    import numpy as np
    import cv2

    # height , width
    img = np.zeros([100,50,3],dtype=np.uint8)
    # x , y
    cv2.rectangle(img, (0,0), (49,99) , (0,0,0,255), -1)
    cv2.circle(img, (25,25), 10, (255,0,0), -1)

    signalLights = find_lights(img)
    for signalLight in signalLights:
        print("signal-light","pos",signalLight.getXY(),"radius",round(signalLight.getCircleRadius()), "expansion" , round(signalLight.getExpansion()), "flatness", round(signalLight.getFlatness()), "color",signalLight.getColor())
    
    assert 1 == len(signalLights)
