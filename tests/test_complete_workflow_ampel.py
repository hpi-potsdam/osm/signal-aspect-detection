import pytest
import numpy as np
import cv2
import skimage.io as io

from signal_aspect_detection.signal_class import load_signal
from signal_aspect_detection import detect_lights
from signal_aspect_detection import filter_matches
from signal_aspect_detection import find_aspect


@pytest.mark.parametrize("image,model,signal_aspect", [
        ('data/ks-signals/traffic_light0001.png','models/signals/ampel.json','green'),
        ('data/ks-signals/traffic_light0002.png','models/signals/ampel.json','green'),
        ('data/ks-signals/traffic_light0003.png','models/signals/ampel.json','yellow + red'),
        ('data/ks-signals/traffic_light0004.png','models/signals/ampel.json','red'),
        ('data/ks-signals/traffic_light0005.png','models/signals/ampel.json','red'),
        ('data/ks-signals/traffic_light0006.png','models/signals/ampel.json','red'),
        ('data/ks-signals/traffic_light0007.png','models/signals/ampel.json','red'),
        ('data/ks-signals/traffic_light0008.png','models/signals/ampel.json','yellow'),
        ('data/ks-signals/traffic_light0009.png','models/signals/ampel.json','yellow')
    ])

def test(image,model,signal_aspect):

    print("image",image)
    I = io.imread(image)

    print("model",model)
    signal_definition = load_signal(model)

    signalLights ,  lights_matches , images  = detect_lights(I,signal_definition)
    
    #dump
    for i in range(len(signalLights)):
        signalLight = signalLights[i]
        print("Light",i,signalLight.getXY(),signalLight.getExpansion(),signalLight.getColor())
        for lm in lights_matches[i]:
            print("     match",lm)

    lights_filtered = filter_matches(lights_matches,0.15,0.7,0.8)
    print("lights_filtered",lights_filtered)
    matching = find_aspect(lights_filtered,signal_definition)  
    print("matching",matching)

    assert signal_aspect == matching
