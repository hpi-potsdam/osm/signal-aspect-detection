

def test_get_elipse_color():
    from signal_aspect_detection.color_detector import get_ellipse_color
    import numpy as np
    import cv2

    # height , width
    img = np.zeros([100,50,3],dtype=np.uint8)
    # x , y
    cv2.rectangle(img, (0,0), (49,99) , (0,0,0,255), -1)
    ellipse = (25,25) , (10,2) , 0
    cv2.ellipse(img, ellipse, (255,0,0), -1)


    (x , y) , (xax , yax) , angle = ellipse
    color = get_ellipse_color(img,x,y,xax,yax,angle)
    assert 1 == len(color)
    assert "red" == color[0][0]
    assert 1.0 == color[0][1]

def test_get_elipse_color_with_yellow_circle():
    from signal_aspect_detection.color_detector import get_ellipse_color
    import numpy as np
    import cv2

    # height , width
    img = np.zeros([100,50,3],dtype=np.uint8)
    # x , y
    cv2.rectangle(img, (0,0), (49,99) , (0,0,0,255), -1)
    ellipse = (25,25) , (10,10) , 0
    cv2.ellipse(img, ellipse, (255,0,0), -1)
    cv2.circle(img, (25,25), 3 , (255,255,0), -1)


    (x , y) , (xax , yax) , angle = ellipse
    color  = get_ellipse_color(img,x,y,xax,yax,angle)

    assert 2 == len(color)
    assert "red" == color[0][0]
    assert 0.6 < color[0][1]


def test_get_elipse_color_for_rotated_ellipse():
    from signal_aspect_detection.color_detector import get_ellipse_color
    import numpy as np
    import cv2

    # height , width
    img = np.zeros([100,50,3],dtype=np.uint8)
    # x , y
    cv2.rectangle(img, (0,0), (49,99) , (0,0,0,255), -1)
    ellipse = (25,25) , (10,2) , 90
    cv2.ellipse(img, ellipse, (255,0,0), -1)


    (x , y) , (xax , yax) , angle = ellipse
    color  = get_ellipse_color(img,x,y,xax,yax,angle)
    assert 1 == len(color)
    assert "red" == color[0][0]
    assert 1.0 == color[0][1]


def test_get_elipse_color_for_small_ellipse():
    from signal_aspect_detection.color_detector import get_ellipse_color
    import numpy as np
    import cv2

    # height , width
    img = np.zeros([100,50,3],dtype=np.uint8)
    cv2.rectangle(img, (0,0), (49,99) , (0,0,0,255), -1)

    ellipse = (0.9898918271064758 ,58.15118408203125) , (2.8634891510009766, 9.053509712219238) , 1.128983974456787
    (x , y) , (xax , yax) , angle = ellipse
    color = get_ellipse_color(img,x,y,xax,yax,angle)
    assert 1 == len(color)
    assert "black" == color[0][0]
    assert 1.0 == color[0][1]
